"""Temporary script to download socketxp files"""

import argparse
import os
import pathlib
import shutil
import sys
import tempfile
import urllib.request

from pathlib import Path

import requests

_common_path: Path = None

SOCKETXP_ARM_URL = "https://portal.socketxp.com/download/arm/socketxp"
# SOCKETXP_EXE_LOCATION = "/usr/bin/socketxp"
# DEVICE_KEY_LOCATION = "/var/lib/socketxp/device.key"
# CONFIG_JSON_LOCATION = "/etc/socketxp/config.json"


def get_common_path() -> str:
    global _common_path
    if _common_path is None:
        home_path = os.path.expanduser("~")
        default_common_path = os.path.join(
            os.environ.get("HOME", home_path), ".apps", "common"
        )
        _common_path = Path(os.environ.get("COMMON_PATH", default_common_path))
    return _common_path


def get_device_key_and_config_json(base_url, name):
    url = f"{base_url}/new_device/{name}"
    try:
        req = requests.get(url)

        if not req.ok:
            print("Error getting request data.")
            print(f"status_code={req.status_code}")
            sys.exit(req.text)

        return req.json()
    except requests.RequestException as exception:
        sys.exit(f"Error reading server, check if server is running.\n{exception}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--base-url", help="base server url")
    parser.add_argument("-n", "--name", help="device name")
    args = parser.parse_args()

    if not args.name:
        with open("/proc/cpuinfo", "r") as cpuinfo:
            for line in cpuinfo.readlines():
                if 'Serial' in line:
                    split = line.split(":")
                    if len(split) > 1:
                        name = split[1].strip()
                        break
        if not name:
            sys.exit("Error: cannot find serial number from /proc/cpuinfo.")

    if not args.base_url or not name:
        parser.print_help()
        sys.exit("\nError: missing argument.")

    data = get_device_key_and_config_json(args.base_url, name)

    device_key_content = data["device.key"]
    config_json_content = data["config.json"]

    with tempfile.TemporaryDirectory() as tmpdirname:

        tmpdir = pathlib.Path(tmpdirname)

        print("Downloading files please wait...")
        urllib.request.urlretrieve(SOCKETXP_ARM_URL, tmpdir / "socketxp")

        with open(tmpdir / "device.key", "w", encoding="utf-8") as device_key_file:
            device_key_file.write(device_key_content)

        with open(tmpdir / "config.json", "w", encoding="utf-8") as config_json_file:
            config_json_file.write(config_json_content)

        output_dir = get_common_path() / "socketxp"
        output_dir.mkdir(exist_ok=True)

        shutil.copy(tmpdir / "socketxp", output_dir)
        shutil.copy(tmpdir / "device.key", output_dir)
        shutil.copy(tmpdir / "config.json", output_dir)

        print(f"Files downloaded on {output_dir.name}")
