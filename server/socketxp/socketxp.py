"""CLI interface to SocketXP"""

import argparse
import queue
import shutil
import subprocess
import sys

from pathlib import Path
from threading import Thread

from typing import List

from fastapi import HTTPException

import requests

SOCKETXP_BIN = Path.cwd() / "bin" / "socketxp"
SOCKETXP_DEVICE_KEY_FILENAME = Path("/var/lib/socketxp/device.key")

# TODO: change to 2202
SSH_PORT = 22

class MyArgParser(argparse.ArgumentParser):
    """Extends ArgumentParser to print help when it fails."""

    def error(self, message):
        self.print_help()
        print()
        sys.exit(f"error: {message}\n")


def list_devices(authtoken, silent=False):
    headers = {"Authorization": f"Bearer {authtoken}"}
    url = "https://api.socketxp.com/v1/devices/1/10000"

    req = requests.get(url, headers=headers)

    if not req.ok:
        print("Error getting request data.")
        print(f"status_code={req.status_code}")
        sys.exit(req.text)

    num_devices = req.json()["TotalNumDevices"]
    if num_devices == 0:
        if not silent:
            print("No registered devices found.")
        sys.exit(0)

    devices = {}

    for dev in req.json()["Devices"]:
        id = dev["DeviceId"]
        name = dev["DeviceName"]
        status = dev["DeviceStatus"]

        if not silent:
            print(f"{id} {name} -> {status}")

        devices[id] = name

    return devices


def get_device_id(authtoken, device_name):
    devices = list_devices(authtoken, silent=True)
    for device_id, name in devices.items():
        if device_name == name:
            return device_id
    return ""


def show_device(args):
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {args.authtoken}",
    }
    data = f'[{{"DeviceId":"{args.device_id}"}}]'
    url = "https://api.socketxp.com/v1/devices"

    req = requests.get(url, headers=headers, data=data)

    if not req.ok:
        print("Error getting request data.")
        print(f"status_code={req.status_code}")
        sys.exit(req.text)

    for dev in req.json():
        print("---")
        for key, value in dev.items():
            print(f"{key} : {value}")


def delete_device(args):
    headers = {"Authorization": f"Bearer {args.authtoken}"}
    url = f"https://api.socketxp.com/v1/devices/{args.device_id}"

    devices = list_devices(args.authtoken, silent=True)

    req = requests.delete(url, headers=headers)

    if req.ok:
        print("Successfully deleted")
        if devices[args.device_id]:
            folder = Path.cwd() / devices[args.device_id]
            shutil.rmtree(folder)
            print(f"Removed: {folder}")
        else:
            print(f"{args.device_id} not found on {devices}")
    else:
        print(f"status_code={req.status_code}")
        print(req.text)


def list_tunnels(args):
    headers = {"Authorization": f"Bearer {args.authtoken}"}
    url = "https://api.socketxp.com/v1/tunnels"

    req = requests.get(url, headers=headers)

    if req.ok:
        for tunnel in req.json():
            print("---")
            for key, value in tunnel.items():
                print(f"{key} : {value}")
    else:
        print(f"status_code={req.status_code}")
        print(req.text)


def get_free_port():
    # TODO: implement it!
    return 3001

def call_socketxp_ssh(cmd: List[str], result_queue: queue.Queue):
    # SSH_TIMEOUT_SECS = 5 * 60 # 5 minutes
    SSH_TIMEOUT_SECS = 10000 * 60

    print("[call_socketxp_ssh] running: " + ' '.join(cmd))
    try:
        proc = subprocess.Popen(cmd)
        proc.communicate(timeout=SSH_TIMEOUT_SECS)
        rc = proc.returncode
        if rc:
            print("[call_socketxp_ssh] error calling socketxp")
            result_queue.put(HTTPException(status_code=400,
                    detail="Error calling socketxp"))
    except subprocess.TimeoutExpired:
        # TODO: use netstat to check if ssh is still in use
        # netstat -np | grep ":$PORT.*ESTABLISHED.*socketxp"
        print("[call_socketxp_ssh] timeout: killing socketxp")
        proc.kill()
    except Exception as exception:
        print(exception)
        result_queue.put(HTTPException(status_code=400,
            detail=f"Unkonwn error: {exception}"))

    print("[call_socketxp_ssh] finished")


def open_ssh(device_name, authtoken):

    device_id = get_device_id(authtoken, device_name)
    if not device_id:
        raise HTTPException(status_code=400,
                detail=f"Error: device {device_name} not found")
    port = get_free_port()

    cmd = [str(SOCKETXP_BIN), "connect", f"tcp://0.0.0.0:{port}", "--iot-slave", "--peer-device-id", device_id, "--peer-device-port", "22", "--authtoken", authtoken]

    result_queue = queue.Queue()
    thread = Thread(target = call_socketxp_ssh, args=(cmd, result_queue,))
    thread.start()

    try:
        print("waiting queue")
        exception = result_queue.get(timeout=1)
        if exception:
            raise exception
    except queue.Empty:
        print("No exception found, exiting")
        pass

    print(f"{port=}")

    return port


def create_device(name, authtoken):
    output_dir = Path.cwd() / name
    output_dir.mkdir(exist_ok=True)

    config_json_text = f"""{{
    "tunnels" : [
        {{
            "destination": "tcp://127.0.0.1:{SSH_PORT}"
        }},
        {{
            "destination": "http://127.0.0.1:5000",
            "custom_domain": "",
            "subdomain": "gabriel_tech-{name}"
        }}
    ]
}}"""

    config_json = Path(output_dir / "config.json")
    config_json.write_text(config_json_text, encoding="utf-8")

    headers = {"Authorization": f"Bearer {authtoken}"}
    url = f"https://api.socketxp.com/v1/devices"

    data = f'{{"DeviceName":"{name}"}}'

    req = requests.post(url, headers=headers, data=data)

    if req.ok:
        print("Successfully created")
        device_key_text = req.text
        device_key_file = Path(output_dir / "device.key")
        device_key_file.write_text(device_key_text)
    else:
        print(f"Error creating device: status_code={req.status_code}")
        print(req.text)

    return {"device.key": device_key_text, "config.json": config_json_text}


def _main():
    parser = MyArgParser(
        description=__doc__,
        epilog=f"\nUse `{sys.argv[0]} <cmd> -h` to read about specific commands.",
    )

    subparsers = parser.add_subparsers(dest="cmd")

    parser_create = subparsers.add_parser(
        "create",
        help="creates a new device on the SocketXP platform and create the configuration files on a folder with device_name",
    )
    parser_list = subparsers.add_parser(
        "list", help="list registered devices on SocketXP platform"
    )
    parser_show = subparsers.add_parser(
        "show", help="shows available information for a specific device"
    )
    parser_delete = subparsers.add_parser("delete", help="deletes a device")
    parser_tunnels = subparsers.add_parser(
        "tunnels", help="show all registered tunnels"
    )
    parser_rmtunnels = subparsers.add_parser(
        "tunnels", help="remove tunnels for a given device"
    )

    parser_create.add_argument("name", help="device_name")
    parser_create.add_argument("--authtoken", help="authtoken", required=True)

    parser_list.add_argument("--authtoken", help="authtoken", required=True)

    parser_show.add_argument("device_id", help="device_name")
    parser_show.add_argument("--authtoken", help="authtoken", required=True)

    parser_delete.add_argument("device_id", help="device_name")
    parser_delete.add_argument("--authtoken", help="authtoken", required=True)

    parser_tunnels.add_argument("--authtoken", help="authtoken", required=True)

    parser_rmtunnels.add_argument("device_id", help="device_name")
    parser_rmtunnels.add_argument("--authtoken", help="authtoken", required=True)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit("\nError: command not specified.")

    args = parser.parse_args()

    if args.cmd == "create":
        create_device(args.name, args.authtoken)
    elif args.cmd == "list":
        list_devices(args.authtoken)
    elif args.cmd == "show":
        show_device(args)
    elif args.cmd == "delete":
        delete_device(args)
    elif args.cmd == "tunnels":
        list_tunnels(args)
    elif args.cmd == "rm-tunnels":
        rm_tunnels(args)
    else:
        parser.print_help()
        sys.exit(f"\nError: invalid command '{args.cmd}'")


if __name__ == "__main__":
    _main()
