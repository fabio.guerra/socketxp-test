import argparse
import sys

import uvicorn
from fastapi import FastAPI

from socketxp import socketxp

authtoken = ""

app = FastAPI()


@app.get("/new_device/{device_name}")
async def new_device(device_name: str):
    return socketxp.create_device(device_name, authtoken)


@app.get("/ssh/{device_name}")
async def open_ssh(device_name: str):
    return socketxp.open_ssh(device_name, authtoken)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host")
    parser.add_argument("-p", "--port")
    parser.add_argument("-a", "--authtoken")
    args = parser.parse_args()

    if not args.host or not args.port or not args.authtoken:
        parser.print_help()
        sys.exit("\nError: missing argument.")

    authtoken = args.authtoken

    uvicorn.run(app, host=args.host, port=int(args.port))
