VENV_DIR=venv_su

if [ ! -d "$VENV_DIR" ]; then
    sudo python3 -m venv venv_su
fi

. venv_su/bin/activate

sudo pip3 install -q -r requirements.txt
sudo python3 server.py --host 0.0.0.0 -p 8080 -a eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDY5Mzc4ODUsImlkIjoiZndndWVyYUBnbWFpbC5jb20ifQ.lPTs2_E_h9nfJgD-fW1XuTN9PywfwjAbhTMV-uqVWn0

