set -e

SOCKETXP_BIN_DIR=/usr/local/bin
if [ ! -f "$SOCKETXP_BIN_DIR/socketxp" ]; then
    echo "### Error: $SOCKETXP_BIN_DIR/socketxp does not exists"
    exit 1
fi
sudo chmod +wx $SOCKETXP_COMMON_PATH/socketxp
sudo mv -v $SOCKETXP_COMMON_PATH/socketxp $SOCKETXP_BIN_DIR/socketxp

SOCKETXP_LIB_DIR=/var/lib/socketxp
SOCKETXP_COMMON_PATH=/home/pi/.apps/common/socketxp

if [ ! -d "$SOCKETXP_COMMON_PATH" ]; then
    echo "$SOCKETXP_COMMON_PATH does not exists"
    exit 1
fi

if [ ! -f "$SOCKETXP_COMMON_PATH/config.json" ]; then
    echo "$SOCKETXP_COMMON_PATH/config.json does not exists"
    exit 1
fi

if [ ! -f "$SOCKETXP_COMMON_PATH/device.key" ]; then
    echo "$SOCKETXP_COMMON_PATH/device.key does not exists"
    exit 1
fi

sudo mkdir -p /var/lib/socketxp
sudo mkdir -p /etc/socketxp

sudo mv -v $SOCKETXP_COMMON_PATH/device.key /var/lib/socketxp/device.key
sudo mv -v $SOCKETXP_COMMON_PATH/config.json /etc/socketxp/config.json

# Configure and run SocketXP agent as a linux systemd daemon service
sudo $SOCKETXP_BIN_DIR/socketxp service install --config /etc/socketxp/config.json
if [ $? -eq 0 ]; then
    echo "+++ SocketXP Service Install Completed"
else 
    echo "### Error: SocketXP Service Install failed!"
    exit 1
fi

sudo systemctl daemon-reload
if [ $? -eq 0 ]; then
    echo "+++ SocketXP Service Daemon-Reload Completed"
else 
    echo "### Error: SocketXP Service daemon-reload failed!"
    exit 1
fi

sudo systemctl start socketxp
if [ $? -eq 0 ]; then
    echo "+++ SocketXP Service Started"
else 
    echo "### Error: SocketXP Service start failed!"
    exit 1
fi

sudo systemctl enable socketxp
if [ $? -eq 0 ]; then
    echo "+++ Enabled SocketXP Service to start always on reboot"
else 
    echo "### Error: SocketXP Service enable failed!"
    exit 1
fi
